jQuery( document ).ready(function() {
		
		var back =jQuery(".prev");
		var	next = jQuery(".next");
		var	steps = jQuery(".step");
		var	stepsDivs = jQuery(".steps");
		
		next.bind("click", function() { 
			jQuery.each( steps, function( i ) {
				if (!jQuery(steps[i]).hasClass('current') && !jQuery(steps[i]).hasClass('done')) {
					jQuery(steps[i]).addClass('current');
					jQuery(steps[i - 1]).removeClass('current').addClass('done');
					jQuery(stepsDivs[i]).addClass('current');
					jQuery(stepsDivs[i - 1]).removeClass('current').addClass('done');
					return false;
				}
			})		
		});
		back.bind("click", function() { 
			jQuery.each( steps, function( i ) {
				if (jQuery(steps[i]).hasClass('done') && jQuery(steps[i + 1]).hasClass('current')) {
					jQuery(steps[i + 1]).removeClass('current');
					jQuery(steps[i]).removeClass('done').addClass('current');
					jQuery(stepsDivs[i + 1]).removeClass('current');
					jQuery(stepsDivs[i]).removeClass('done').addClass('current');
					return false;
				}
			})		
		});
			jQuery('.table.table-bordered').tableHeadFixer({'head': true,'foot':true});
			jQuery('[data-rel=popover]').popover({html:true});
		
	})