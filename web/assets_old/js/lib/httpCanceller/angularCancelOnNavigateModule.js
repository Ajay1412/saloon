angular
  .module('angularCancelOnNavigateModule', ['cfp.loadingBar'])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('HttpRequestTimeoutInterceptor');
  })
  .run(function ($rootScope, HttpPendingRequestsService) {
    $rootScope.$on('$locationChangeSuccess', function (event, newUrl, oldUrl) {
       //console.log("1111");
      if (newUrl != oldUrl) {

        HttpPendingRequestsService.cancelAll();
      }
    })
  });
