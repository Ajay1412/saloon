AmCharts.themes.light = {

	AmChart: {
		color: "#C0D3EC"
	},

	AmCoordinateChart: {
		colors: ["#67b7dc", "#fdd400", "#84b761", "#cc4748", "#cd82ad", "#2f4074", "#448e4d", "#b7b83f", "#b9783f", "#b93e3d", "#913167"]
	},

	AmPieChart: {
		colors: ["#67b7dc", "#fdd400", "#84b761", "#cc4748", "#cd82ad", "#2f4074", "#448e4d", "#b7b83f", "#b9783f", "#b93e3d", "#913167"]
	},

	AmStockChart: {
		colors: ["#67b7dc", "#fdd400", "#84b761", "#cc4748", "#cd82ad", "#2f4074", "#448e4d", "#b7b83f", "#b9783f", "#b93e3d", "#913167"]
	},

	AmSlicedChart: {
		outlineAlpha: 1,
		outlineThickness: 2,
		labelTickColor: "#C0D3EC",
		labelTickAlpha: 0.3
	},

	AmRectangularChart: {
		zoomOutButtonColor: '#C0D3EC',
		zoomOutButtonRollOverAlpha: 0.15,
		zoomOutButtonImage: "lens.png"
	},

	AxisBase: {
		axisColor: "#C0D3EC",
		axisAlpha: 0.3,
		gridAlpha: 0.1,
		gridColor: "#C0D3EC"
	},

	ChartScrollbar: {
		backgroundColor: "#C0D3EC",
		backgroundAlpha: 0.2,
		graphFillAlpha: 0.5,
		graphLineAlpha: 0,
		selectedBackgroundColor: "#FFFFFF",
		selectedBackgroundAlpha: 0.25,
		gridAlpha: 0.15
	},

	ChartCursor: {
		cursorColor: "#C0D3EC",
		color: "#FFFFFF",
		cursorAlpha: 0.5
	},

	AmLegend: {
		color: "#C0D3EC"
	},

	AmGraph: {
		lineAlpha: 0.9
	},


	GaugeArrow: {
		color: "#C0D3EC",
		alpha: 0.8,
		nailAlpha: 0,
		innerRadius: "40%",
		nailRadius: 15,
		startWidth: 15,
		borderAlpha: 0.8,
		nailBorderAlpha: 0
	},

	GaugeAxis: {
		tickColor: "#C0D3EC",
		tickAlpha: 1,
		tickLength: 15,
		minorTickLength: 8,
		axisThickness: 3,
		axisColor: '#C0D3EC',
		axisAlpha: 1,
		bandAlpha: 0.8
	},

	TrendLine: {
		lineColor: "#c03246",
		lineAlpha: 0.8
	},

	// ammap
	AreasSettings: {
		alpha: 0.8,
		color: "#C0D3EC",
		colorSolid: "#C0D3EC",
		unlistedAreasAlpha: 0.4,
		unlistedAreasColor: "#C0D3EC",
		outlineColor: "#fff",
		outlineAlpha: 0.5,
		outlineThickness: 0.5,
		rollOverColor: "#56B7F7",
		rollOverOutlineColor: "#FFFFFF",
		selectedOutlineColor: "#FFFFFF",
		selectedColor: "#1C84C6",
		unlistedAreasOutlineColor: "#FFFFFF",
		unlistedAreasOutlineAlpha: 0.5
	},

	LinesSettings: {
		color: "#C0D3EC",
		alpha: 0.8
	},

	ImagesSettings: {
		alpha: 0.8,
		labelColor: "#C0D3EC",
		color: "#C0D3EC",
		labelRollOverColor: "#3c5bdc"
	},

	ZoomControl: {
		buttonRollOverColor: "#A6A6A6",
		buttonFillColor: "#BEBEBE",
		buttonFillAlpha: 0.8,
		buttonBorderColor: "#C0D3EC",
		gridBackgroundColor: "#C0D3EC"
	},

	SmallMap: {
		mapColor: "#C0D3EC",
		rectangleColor: "#f15135",
		backgroundColor: "#FFFFFF",
		backgroundAlpha: 0.7,
		borderThickness: 1,
		borderAlpha: 0.8
	},

	// the defaults below are set using CSS syntax, you can use any existing css property
	// if you don't use Stock chart, you can delete lines below
	PeriodSelector: {
		color: "#C0D3EC"
	},

	PeriodButton: {
		color: "#C0D3EC",
		backgroundColor: "#FFFFFF",
		borderStyle: "solid",
		borderColor: "#a9a9a9",
		borderWidth: "1px",
		MozBorderRadius: "5px",
		borderRadius: "5px",
		margin: "1px",
		outline: "none"
	},

	PeriodButtonSelected: {
		color: "#C0D3EC",
		backgroundColor: "#b9cdf5",
		borderStyle: "solid",
		borderColor: "#b9cdf5",
		borderWidth: "1px",
		MozBorderRadius: "5px",
		borderRadius: "5px",
		margin: "1px",
		outline: "none"
	},

	PeriodInputField: {
		background: "transparent",
		borderStyle: "solid",
		borderColor: "#a9a9a9",
		borderWidth: "1px",
		outline: "none"
	},

	DataSetSelector: {
		selectedBackgroundColor: "#b9cdf5",
		rollOverBackgroundColor: "#666"
	},

	DataSetCompareList: {
		borderStyle: "solid",
		borderColor: "#a9a9a9",
		borderWidth: "1px"
	},

	DataSetSelect: {
		borderStyle: "solid",
		borderColor: "#a9a9a9",
		borderWidth: "1px",
		outline: "none"
	}

};