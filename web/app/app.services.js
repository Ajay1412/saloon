angular.module('saloon').factory('aclService', ['$http', '$cookieStore', '$rootScope', '$timeout', 'CONFIG', '$filter', 
    function ($http, $cookieStore, $rootScope, $timeout, CONFIG, $filter) {
        var service = {};
        
        /* ACL Validate Function */
        service.checkACL = function(Module,SubModule,Access){
            return true;
            // var data = $filter('filter')($rootScope.globals.UserAccess,function(obj){
            //     if(obj.Module == Module && obj.SubModule == SubModule && Access.indexOf(obj.Access) !== -1){
            //         return obj;
            //     }
            // });

            // if(data != undefined && data.length){
            //     return true;
            // } else {
            //     return false;
            // }
        }

        service.setACLRequestHeader = function(Module,SubModule){
            // var data = $filter('filter')($rootScope.globals.UserAccess,function(obj){
            //     if(obj.Module == Module && obj.SubModule == SubModule){
            //         return obj;
            //     }
            // });

            // if(data != undefined && data.length){
            //     $http.defaults.headers.common['SubModRef'] = data[0].SubModRef;
            //     $http.defaults.headers.common['Access'] = data[0].Access;
            //     return data[0]; 
            // } else {
            //     return false;
            // }
        };

        service.unsetACLRequestHeader = function(Module,SubModule){
               delete $http.defaults.headers.common['SubModRef'] ;
               delete $http.defaults.headers.common['Access'] ;
        };

        service.setACL = function(){

        };

        
        return service;
    }
]);

angular.module('saloon').factory('commanService', ['$q','$http', '$cookieStore', '$rootScope', '$timeout', 'CONFIG', '$filter', '$localStorage','$sce','$location','$ngConfirm',
    function ($q,$http, $cookieStore, $rootScope, $timeout, CONFIG, $filter, $localStorage,$sce,$location,$ngConfirm) {
        var service = {};
        var filters = [];

        service.clearLogin =  function(){
            delete $rootScope.globals;
            delete $localStorage.globals;
            $location.path('/');
        };

        service.formErrorFocus = function(){
            $timeout(function() {
                if(angular.element(".has-error:eq(0) input").length){
                    angular.element(".has-error:eq(0) input").focus();
                }else if(angular.element(".has-error:eq(0) select").length){
                    angular.element(".has-error:eq(0) select").focus();
                }else if(angular.element(".has-error:eq(0) textarea").length){
                    angular.element(".has-error:eq(0) textarea").focus();
                }
            }, 0);
        };

        service.formReset = function(form_obj){
            form_obj.submitted = false;
            form_obj.$setPristine();
            form_obj.$setUntouched();
        };

        service.exportExcel = function(id,filename){
            var blob = new Blob([document.getElementById(id).innerHTML], {
                    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
            });
            saveAs(blob, filename);
        };

        service.DateDiff = function(date1,date2) {
            var k = date1.split("/");
            var date1 = k[1]+'/'+k[0]+'/'+k[2];

            var k = date2.split("/");
            var date2 = k[1]+'/'+k[0]+'/'+k[2];

            date1 = new Date(date1);
            date2 = new Date(date2);
            var diff = Math.floor(date2.getTime() - date1.getTime());
            var day = 1000 * 60 * 60 * 24;

            var days = Math.floor(diff/day);
           
            var y = 365;
            var y2 = 30;
            var years = days % y;
            var months = years % y2;
            year = (days - years) / y;
            month = (years - months) / y2;

            var message = year + ".";
            message += month;
            return message
        }

        service.viewDocument = function (path,title) {
            $rootScope.document_show_popup = true;
            $rootScope.linkTitle = title;
            $rootScope.linkShow = $sce.trustAsResourceUrl(path);
        }

        
        /* Countries List */
        service.get_countries = function(){
            var deferred = $q.defer();
            $http.post(CONFIG.apiURL + 'user/countries',{})
            .success(function(response) {
                if(response.message_type == 'success'){
                    deferred.resolve(response);
                } else {
                    deferred.reject(response);
                }
            })
            .error(function(error, status) {
                deferred.reject({
                    message : "Network Error",
                    message_type : 'error'
                });
            });

            return deferred.promise;
        }

        /* States List on bases of country_id */
        service.get_states = function(country_id){
            var deferred = $q.defer();
            $http.post(CONFIG.apiURL + 'user/states',{
                country_id : country_id
            })
            .success(function(response) {
                if(response.message_type == 'success'){
                    deferred.resolve(response);
                } else {
                    deferred.reject(response);
                }
            })
            .error(function(error, status) {
                deferred.reject({
                    message : "Network Error",
                    message_type : 'error'
                });
            });

            return deferred.promise;
        }

        /* Cities List on bases of state_id */
        service.get_cities = function(state_id){
            var deferred = $q.defer();
            $http.post(CONFIG.apiURL + 'user/cities',{
                state_id : state_id
            })
            .success(function(response) {
                if(response.message_type == 'success'){
                    deferred.resolve(response);
                } else {
                    deferred.reject(response);
                }
            })
            .error(function(error, status) {
                deferred.reject({
                    message : "Network Error",
                    message_type : 'error'
                });
            });

            return deferred.promise;
        }

        /* Show Error Message */
        service.showMessage = function(message,message_type){

            if(message_type == 'error'){
                $ngConfirm({
                    title: 'Encountered an error!',
                    content: message,
                    type: 'red',
                    typeAnimated: true,
                    buttons: {
                        tryAgain: {
                            text: 'Try again',
                            btnClass: 'btn-red',
                            action: function(){
                            }
                        },
                        close: function () {
                        }
                    }
                });
            }

            if(message_type == 'success'){
                $ngConfirm({
                    title: 'Congratulations!',
                    type: 'green',
                    content: message,
                    buttons: {
                        ok: {
                            btnClass: "btn-green",
                        }
                    }
                })
            }
        };
    
        
        /* Genders Masters */
        service.Genders = function(){
            var Genders = { 
                'Male' : 'Male',
                'Female' : 'Female',
            }
            return Genders;
        };

        
        /* Days difference in two dates */
        service.daysDiff = function(date1,date2){
            var timeDiff = Math.abs(date1.getTime() - date2.getTime());   
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            return diffDays;
        };
        
        /* Return months from Year */
        service.getMonths = function () {
            var months = {
                1: 'Jan',
                2: 'Feb',
                3: 'Mar',
                4: 'Apr',
                5: 'May',
                6: 'jun',
                7: 'jul',
                8: 'Aug',   
                9: 'Sep',
                10: 'Oct',
                11: 'Nov',
                12: 'Dec',
            }
            return months;
        };
        
        service.setFilters = function (filterObj) {
            filters = [];
            filters.push(filterObj);
        };

        service.getFilters = function () {
            return filters;
        };

        service.ifNull = function(obj){
            var temp = false;
            if (obj == null){
                temp = true;
            } else if (obj == undefined){
                temp = true;
            } else if ( Object.keys(obj).length === 0) {
                temp = true;
            }
            return temp;
        };

        return service;
    }]);

       