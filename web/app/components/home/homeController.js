angular.module('saloon').controller('homeController', ['$scope', '$rootScope', '$location','commanService', 'homeService', '$timeout', '$filter',
    function ($scope, $rootScope, $location, commanService, homeService, $timeout, $filter) {
        $scope.current_date = new Date();

        $scope.country_change = function(){
        	commanService.get_states($scope.form_saloon.country_id).then(function (response) {
        		$scope.states = response.data.states;
        		$scope.form_saloon.state_id = undefined;
        		$scope.form_saloon.city_id = undefined;
        	},function (response) {
        		$scope.states = undefined;
        		$scope.form_saloon.state_id = undefined;
        		$scope.form_saloon.city_id = undefined;
        	});
        };

        $scope.state_change = function(){
        	commanService.get_cities($scope.form_saloon.state_id).then(function (response) {
        		$scope.cities = response.data.cities;
        		$scope.form_saloon.city_id = undefined;
        	},function (response) {
        		$scope.cities = undefined;
        		$scope.form_saloon.city_id = undefined;
        	});
        };

        $scope.submit = function(){
        	blockUI.start();
        	commanService.saloon_insert($scope.form_saloon).then(function (response) {
        		blockUI.stop();	
        	},function (response) {
        		blockUI.stop();
        	});
        };

        // Call When view loaded
        $scope.$on('$viewContentLoaded', function (event) {
			commanService.get_countries().then(function (response) {
        		$scope.countries = response.data.countries;
        	},function (response) {
        		$scope.countries = undefined;
        	});
        });
    }
]);
