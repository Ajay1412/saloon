angular.module('saloon').factory('homeService', ['$http', '$cookieStore', '$rootScope', '$timeout', 'CONFIG',
    function ($http, $cookieStore, $rootScope, $timeout, CONFIG) {
        var service = {};
        var filters = [];
        
        
        service.setFilters = function(filterObj) {
            filters = [];
            filters.push(filterObj);
        };

        service.getFilters = function(){
            return filters;
        };

        return service;
    }]);

       