angular.module('saloon').factory('saloonService', ['$q','$http', '$cookieStore', '$rootScope', '$timeout', 'CONFIG',
    function ($q,$http, $cookieStore, $rootScope, $timeout, CONFIG) {
        var service = {};
        var filters = [];
        
        service.saloon_list = function(data){
            var deferred = $q.defer();
            $http.post(CONFIG.apiURL + 'saloon/list',data)
            .success(function(response) {
                if(response.message_type == 'success'){
                    deferred.resolve(response);
                } else {
                    deferred.reject(response);
                }
            })
            .error(function(error, status) {
                deferred.reject({
                    message : "Network Error",
                    message_type : 'error'
                });
            });

            return deferred.promise;
        }

        /* Cities List on bases of state_id */
        service.saloon_insert = function(data){
            var deferred = $q.defer();
            $http.post(CONFIG.apiURL + 'saloon/insert',data)
            .success(function(response) {
                if(response.message_type == 'success'){
                    deferred.resolve(response);
                } else {
                    deferred.reject(response);
                }
            })
            .error(function(error, status) {
                deferred.reject({
                    message : "Network Error",
                    message_type : 'error'
                });
            });

            return deferred.promise;
        }

        service.setFilters = function(filterObj) {
            filters = [];
            filters.push(filterObj);
        };

        service.getFilters = function(){
            return filters;
        };

        return service;
    }]);

       