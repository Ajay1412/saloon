angular.module('saloon').controller('saloonListController', ['$scope', '$rootScope', '$location','commanService', 'saloonService', '$timeout', '$filter','blockUI','CONFIG',
    function ($scope, $rootScope, $location, commanService, saloonService, $timeout, $filter,blockUI,CONFIG ) {
        $scope.current_date = new Date();
        $scope.filters = {};

        $scope.init = function(){
            $scope.filters.page = 1;
            $scope.filters.limit= CONFIG.limit;
            $scope.filters.limit_options = CONFIG.limit_options;
            $scope.filters.order_by = 'saloon_name';
            $scope.filters.order = 'asc';
        };

        $scope.country_change = function(){
        	commanService.get_states($scope.form_saloon.country_id).then(function (response) {
        		$scope.states = response.data.states;
        		$scope.form_saloon.state_id = undefined;
        		$scope.form_saloon.city_id = undefined;
        	},function (response) {
        		$scope.states = undefined;
        		$scope.form_saloon.state_id = undefined;
        		$scope.form_saloon.city_id = undefined;
        	});
        };

        $scope.state_change = function(){
        	commanService.get_cities($scope.form_saloon.state_id).then(function (response) {
        		$scope.cities = response.data.cities;
        		$scope.form_saloon.city_id = undefined;
        	},function (response) {
        		$scope.cities = undefined;
        		$scope.form_saloon.city_id = undefined;
        	});
        };

        $scope.submit = function(){
        	blockUI.start();
        	saloonService.saloon_insert($scope.form_saloon).then(function (response) {
        		blockUI.stop();	
                $scope.popup_saloon_add_show_hide(false);
        	},function (response) {
        		blockUI.stop();
                $scope.popup_saloon_add_show_hide(false);
        	});
        };

        $scope.add_saloon = function() {
            if($scope.popup_saloon_add){
                $scope.popup_saloon_add_show_hide(false);
               
            } else { 
                commanService.formReset($scope.formsaloonadd);
                $scope.popup_saloon_add_show_hide(true);
            }
        };

        $scope.get_saloon_list = function(){
            blockUI.start();
            saloonService.saloon_list($scope.filters).then(function (response) {
                $scope.saloon_list = response.data
                blockUI.stop(); 
            },function (response) {
                blockUI.stop();
            });
        };

        $scope.reset_pagination = function(){
            $scope.filters.page = 1;
            $scope.get_saloon_list();
        };

        $scope.popup_saloon_add_show_hide = function(val){
            $scope.popup_saloon_add = val
        };

        // Call When view loaded
        $scope.$on('$viewContentLoaded', function (event) {
			commanService.get_countries().then(function (response) {
        		$scope.countries = response.data.countries;
        	},function (response) {
        		$scope.countries = undefined;
        	});

            $scope.init();
            $scope.get_saloon_list();
        });
    }
]);
