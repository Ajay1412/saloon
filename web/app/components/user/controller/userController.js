angular.module('saloon').controller('userController', ['$scope', '$rootScope', '$location', 'userService', 'commanService', '$remember','blockUI',
    function($scope, $rootScope, $location, userService, commanService, $remember,blockUI) {
        $scope.form_login = {};
        $scope.form_forget_password = {};
        $scope.form_register = {};
        $scope.remember = false;
        
        if ($remember('username') && $remember('password')) {
            $scope.remember = true;
            $scope.username = $remember('username');
            $scope.password = $remember('password');
        }

        $scope.rememberMe = function() {
            if ($scope.remember) {
                $remember('username', $scope.username);
                $remember('password', $scope.password);
            } else {
                $remember('username', '');
                $remember('password', '');
            }
        };

        
        $scope.login = function() {
            $scope.rememberMe();
            blockUI.start();
            var data = $scope.form_login;

            userService.Login(data, function(response) {
                userService.ClearCredentials();
                blockUI.stop();
                userService.SetCredentials(response.data);
                $location.path('/home');
            }, function(response) {
                blockUI.stop();
                commanService.showMessage(response.message, response.message_type);
            });
        };

        $scope.forget_password = function() {
            
            commanService.showHideLoader(true);
            var data = $scope.form_forget_password;

            userService.Login(data, function(response) {
                $location.path('/login');
            }, function(response) {
                commanService.showHideLoader(false);
                commanService.showMessage(response.message, response.message_type);
            });
        };

        $scope.register = function() {
            
            commanService.showHideLoader(true);
            var data = $scope.form_register;

            userService.Login(data, function(response) {
                $location.path('/login');
            }, function(response) {
                commanService.showHideLoader(false);
                commanService.showMessage(response.message, response.message_type);
            });
        };

        $scope.test = function(){

            
        }

    }
]);



angular.module('saloon').factory('$remember', function() {
    function fetchValue(name) {
        var gCookieVal = document.cookie.split("; ");
        for (var i = 0; i < gCookieVal.length; i++) {
            // a name/value pair (a crumb) is separated by an equal sign
            var gCrumb = gCookieVal[i].split("=");
            if (name === gCrumb[0]) {
                var value = '';
                try {
                    value = angular.fromJson(gCrumb[1]);
                } catch (e) {
                    value = unescape(gCrumb[1]);
                }
                return value;
            }
        }
        // a cookie with the requested name does not exist
        return null;
    }
    return function(name, values) {
        if (arguments.length === 1) return fetchValue(name);
        var cookie = name + '=';
        if (typeof values === 'object') {
            var expires = '';
            cookie += (typeof values.value === 'object') ? angular.toJson(values.value) + ';' : values.value + ';';
            if (values.expires) {
                var date = new Date();
                date.setTime(date.getTime() + (values.expires * 24 * 60 * 60 * 1000));
                expires = date.toGMTString();
            }
            cookie += (!values.session) ? 'expires=' + expires + ';' : '';
            cookie += (values.path) ? 'path=' + values.path + ';' : '';
            cookie += (values.secure) ? 'secure;' : '';
        } else {
            cookie += values + ';';
        }
        document.cookie = cookie;
    }
});