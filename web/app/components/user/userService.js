angular.module('saloon').factory('userService', ['Base64', '$http', '$cookieStore', '$rootScope', '$timeout', 'CONFIG', '$sessionStorage', '$q', 'commanService','$localStorage','$location',
    function(Base64, $http, $cookieStore, $rootScope, $timeout, CONFIG, $sessionStorage, $q, commanService,$localStorage,$location) {
        var service = {};

        
        service.Login = function(data, successCallBack, errorCallBack) {

            /* Use this for real authentication
             ----------------------------------------------*/
            $http.post(CONFIG.apiURL + 'user/login', data)
                .success(function(response) {
                    if(response.message_type == 'success'){
                        successCallBack(response);
                    } else {
                        errorCallBack(response);
                    }
                    
                })
                .error(function(response, status) {
                    commanService.showHideLoader(false);
                    commanService.showMessage("Server is not working","error");
                });


        };

        service.SetCredentials = function(LoginUserData) {
            //var authdata = Base64.encode(username + ':' + password);
            $localStorage.globals = $rootScope.globals = LoginUserData;
            $http.defaults.headers.common['access_token'] = $localStorage.globals.user_info.access_token;
            $localStorage.globals.isLogin =  1;
        };

        service.ClearCredentials = function() {
            $rootScope.globals = {};
            delete $localStorage.globals ;
            $rootScope.globals.isLogin =  0;
            $http.defaults.headers.common.access_token = '';
        };

        service.Logout = function() {
           var deferred = $q.defer();
            $http.post(CONFIG.apiURL + 'user/logout')
                .success(function(response) {
                    if(response.message_type == 'success'){
                        delete $rootScope.globals;
                        delete $localStorage.globals;
                        $location.path('/login');
                    }
                    
                    deferred.resolve(response);
                });
            
            return deferred.promise;
        };

        service.Settings = function() {
            $http.defaults.headers.common['SubModRef'] = 0;
            $http.defaults.headers.common['Access'] = 2;
            var deferred = $q.defer();
            if ($sessionStorage.settingData == undefined) {
                $http.post(CONFIG.apiURL + 'user/masters')
                    .success(function(response) {
                        var data = response.Data;

                        

                       
                       
                        $sessionStorage.settingData = data;
                        deferred.resolve(data);
                    })
                    .error(function(error, status) {
                        if(status == 401){
                            commanService.clearLogin();
                        };
                    });
            } else {
                deferred.resolve($sessionStorage.settingData);
            }

            return deferred.promise;
        };

        return service;
    }
])

.factory('Base64', function() {
    /* jshint ignore:start */

    var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

    return {
        encode: function(input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output +
                    keyStr.charAt(enc1) +
                    keyStr.charAt(enc2) +
                    keyStr.charAt(enc3) +
                    keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);

            return output;
        },
        decode: function(input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

            do {
                enc1 = keyStr.indexOf(input.charAt(i++));
                enc2 = keyStr.indexOf(input.charAt(i++));
                enc3 = keyStr.indexOf(input.charAt(i++));
                enc4 = keyStr.indexOf(input.charAt(i++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                output = output + String.fromCharCode(chr1);

                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }

                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";

            } while (i < input.length);

            return output;
        }
    };

    /* jshint ignore:end */
});
