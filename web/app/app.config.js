angular.module('saloon').config([
    '$httpProvider',function($httpProvider) {
    	// Set To Send & Get Cookies Ajax Request with access token
        $httpProvider.defaults.withCredentials = true;
    }
]);
