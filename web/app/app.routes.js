angular.module('saloon').config(function ($routeProvider, $stateProvider) {
    $routeProvider
    	.when('/login', {
    	    templateUrl: 'app/components/user/view/loginView.html',
    	    controller: 'userController',
    	    pageType: 'login',
    	    title: 'Login'
    	})
		.when('/forget-password', {
		    templateUrl: 'app/components/user/view/forgetPasswordView.html',
		    controller: 'userController',
		    pageType: 'forget-password',
		    title: 'Forget Password'
		})
		.when('/register', {
		    templateUrl: 'app/components/user/view/registerView.html',
		    controller: 'userController',
		    pageType: 'register',
		    title: 'Register'
		})
		.when('/home', {
		    templateUrl: 'app/components/home/homeView.html',
		    controller: 'homeController',
		    pageType: 'home',
		    title: 'Home'
		})
		.when('/saloon-list', {
		    templateUrl: 'app/components/saloon/saloonListView.html',
		    controller: 'saloonListController',
		    pageType: 'saloon',
		    title: 'saloon'
		});
    
    // if none of the above states are matched, use this as the fallback
    $routeProvider.otherwise('/login');
});