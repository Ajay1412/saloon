angular.module('saloon')
    // Add Rs icon with price
    .filter('INR', function() {
        return function(input) {
            //  console.log(input);
            if (angular.isUndefined(input) || input === null) {
                return "--";
            } else if (!isNaN(input)) {
                var currencySymbol = '<i class="fa fa-inr"></i>';

                //  input = parseFloat(Math.round(parseInt(input) * 100) / 100).toFixed(2);
                //var output = Number(input).toLocaleString('en-IN');   <-- This method is not working fine in all browsers!           
                input = Math.round(input);
                var result = input.toString().split('.');

                var lastThree = result[0].substring(result[0].length - 3);
                var otherNumbers = result[0].substring(0, result[0].length - 3);

                if (otherNumbers != '' && otherNumbers != '-')
                    lastThree = ',' + lastThree;
                var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

                if (result.length > 1) {
                    output += "." + result[1];
                }

                return currencySymbol + ' ' + output;
            }
        }
    })
    // Add Rs icon with price
    .filter('CUSTOMNUMBER', function() {
        return function(input) {
            if (angular.isUndefined(input) || input === null) {
                return "--";
            } else if (!isNaN(input)) {
                input = Math.round(input);
                var result = input.toString().split('.');
                var lastThree = result[0].substring(result[0].length - 3);
                var otherNumbers = result[0].substring(0, result[0].length - 3);
                if (otherNumbers != '' && otherNumbers != '-')
                    lastThree = ',' + lastThree;
                var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

                if (result.length > 1) {
                    output += "." + result[1];
                }

                return output;
            }
        }
    })
    
    // Add % with Price
    .filter('percentage', ['$window', function($window) {
        return function(input, decimals, suffix) {
            decimals = angular.isNumber(decimals) ? decimals : 2;
            /** if not defined show % in suffix
             * if defined empty then show nothing in suffix
             * if set then show set value
             **/
            if (angular.isUndefined(suffix)) {
                suffix = ' %';
            } else if (suffix == '') {
                suffix = '';
            }

            if ($window.isNaN(input) || !isFinite(input)) {
                return '--';
            }
            return Math.round(input * Math.pow(10, decimals + 2)) / Math.pow(10, decimals) + suffix
        };
    }])
    .filter('POSITIVENEGATIVE', function() {
        return function(input, type, is_invert) {
            var classValue = '';
            is_invert = is_invert || false;
            if (!isFinite(input)) {
                return classValue;
            } else if (input > 0) {
                if (type == 'arrow') {
                    classValue = 'fa-caret-up';
                } else if (type == 'color') {
                    classValue = is_invert ? 'negative' : 'positive';
                }
            } else if (input < 0) {
                if (type == 'arrow') {
                    classValue = 'fa-caret-down';
                } else if (type == 'color') {
                    classValue = is_invert ? 'positive' : 'negative';
                }
            }

            return classValue;
        }
    })
    .filter('CUSTOMDATE', function($filter) {
        return function(input, type, is_invert) {
            var input = $filter('date')(input,'dd/MM/y')
            
            // Show default value to empty.
            if(input == "01/01/1900"){
                input = '';
            }
            return input;
        }
    })
    .filter('CUSTOMDATETIME', function($filter) {
        return function(input, type, is_invert) {
            var input = $filter('date')(input,'dd/MM/y h:mm a')
            return input;
        }
    });
