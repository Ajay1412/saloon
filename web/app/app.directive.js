
    // For icheck box design and action
    angular.module('saloon').directive('icheck', function() {
        return function(scope, element, attr) {
            var eleId = attr.value;
            if (typeof attr.id === 'undefined') {
                $(element).attr('id', attr.value);
            } else {
                eleId = attr.id
            }
            $(element).after('<label for=' + eleId + '><span></span></label>');
        }
    })
    .directive("datepicker", function() {
        return {
            restrict: "A",
            require: "ngModel",
            scope: {
                ngModel: "=",
                minDate: "=",
                maxDate: "="
            },
            link: function(scope, elem, attrs, ngModelCtrl) {


                var updateModel = function(dateText) {
                    scope.$apply(function() {
                        ngModelCtrl.$setViewValue(dateText);
                    });
                };
                // console.log(attrs);
                var options = {
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "1900:Y",
                    onSelect: function(dateText) {
                        updateModel(dateText);
                        this.focus();
                    },
                    onClose: function (ele){
                        this.blur();
                    }
                };

                scope.$watch('maxDate', function(newValue, oldValue) {
                    $(elem).datepicker('option', 'maxDate', newValue);
                })

                scope.$watch('minDate', function(newValue, oldValue) {
                    $(elem).datepicker('option', 'minDate', newValue);
                })

                elem.datepicker(options);
            }
        }
    })
    .directive("time", function($timeout) {
        return {
            restrict: "A",
            require: "ngModel",
            scope: {
                ngModel: "=",
                minTime: "=",
                maxTime: "="
            },
            link: function(scope, element, attrs, ngModelCtrl) {
                //console.log(element);
                //console.log(ngModelCtrl);

                $timeout(function() {

                    var options = {
                        show24Hours: false,
                        ampmPrefix: ' ',
                        useMouseWheel: true,
                        showSeconds: false,
                        timeSteps: [1, 1, 1],
                        spinnerImage: ''
                    };


                    if (attrs.timeMin != undefined) {
                        options.minTime = attrs.timeMin;
                    }

                    if (attrs.timeMax != undefined) {
                        options.maxTime = attrs.timeMax;
                    }


                    $(element).timeEntry(options).change(function() {
                        scope.timeModel = $(element).timeEntry('getTime');
                    });


                    scope.$watch('ngModel', function(newValue, oldValue) {
                        $(element).timeEntry('setTime', newValue);
                    });


                    scope.$watch('minTime', function(newValue, oldValue) {
                        $(element).timeEntry('option', 'minTime', newValue);
                    })
                    scope.$watch('maxTime', function(newValue, oldValue) {
                        $(element).timeEntry('option', 'maxTime', newValue);
                    })

                }, 0);


            }
        }
    })
    .directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if (event.which === 13) {
                    scope.$apply(function() {
                        scope.$eval(attrs.ngEnter, { 'event': event });
                    });

                    event.preventDefault();
                }
            });
        };
    })
    .directive('validFile', function() {
        return {
            require: 'ngModel',
            link: function(scope, el, attrs, ngModel) {
                //change event is fired when file is selected
                el.bind('change', function() {
                    scope.$apply(function() {
                        ngModel.$setViewValue(el.val());
                        ngModel.$render();
                    });
                });
            }
        }
    });