angular.module('saloon').directive('header', function () {
    return {
        restrict: 'A', //This menas that it will be used as an attribute and NOT as an element. I don't like creating custom HTML elements
        replace: true,
        scope: {user: '='}, // This is one of the cool things :). Will be explained in post.
        templateUrl: "app/shared/header/headerView.html",
        controller: ['$scope','$rootScope','$filter','$location','$localStorage','aclService','userService', function ($scope,$rootScope, $filter,$location,$localStorage,aclService,userService) {
                // Your behaviour goes here :)
                $scope.toggleSmoothlyMenu = function () { 
                    console.log('menu');
                    $("body").toggleClass("mini-navbar");
                };

                
                $scope.onLogout = function () {
                    console.log('logout');
                    userService.Logout();
                };
            }]
    }
});

