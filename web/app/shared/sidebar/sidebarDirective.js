angular.module('saloon').directive('sidebar', function () {
    return {
        restrict: 'A', //This menas that it will be used as an attribute and NOT as an element. I don't like creating custom HTML elements
        replace: true,
        templateUrl: "app/shared/sidebar/sidebarView.html",
        controller: ['$scope', '$rootScope','$filter','$location', function ($scope, $rootScope, $filter,$location) {
            // Your behaviour goes here :)
            //console.log($rootScope.pageType);
            $scope.pageType = $rootScope.pageType;
            $scope.setType = function (type) {
               $scope.pageType = type;
            };
            $scope.isCurrentPath = function (path) {
                return $location.path() == path;
            };
        }]
    }
});

