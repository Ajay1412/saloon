angular.module('saloon').directive('fileLoader', function () {
    return {
        restrict: 'A', //This menas that it will be used as an attribute and NOT as an element. I don't like creating custom HTML elements
        replace: true,
        scope: {user: '='}, // This is one of the cool things :). Will be explained in post.
        templateUrl: "app/shared/fileLoader/fileLoaderView.html",
        controller: ['$scope', '$filter', function ($scope, $filter,$rootScope) {
            // Your behaviour goes here :)
        }]
    }
});

