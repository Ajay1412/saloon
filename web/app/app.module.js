angular.module('saloon', ['ui.bootstrap','alexjoffroy.angular-loaders', 'ngRoute', 'ui.router', 'ngCookies', 'ngAnimate', 'ngSanitize', 'ngLoadingSpinner', 'ngStorage', 'ngMessages', 'angularValidator', 'file-uploader','cp.ngConfirm','blockUI'])
    .constant('CONFIG', {
        apiURL: "http://localhost/saloon/api/index.php/",
        siteURL: "http://localhost/projects/saloon_new/",
        limit: '10',
        limit_options: ['10', '20'],
        is_production: true
    })
    .run(function($rootScope,$state, $location, $cookieStore, $cookies, $timeout, CONFIG, $localStorage, $http, userService, commanService,aclService) {

        $rootScope.siteURL = CONFIG.siteURL;
       

        /* If user is not logged in then redirect on login page */
        $rootScope.$on('$routeChangeStart', function(event) {
            console.log($location.path());
            // To remove toast message during navigation
            toastr.clear();
           // $localStorage.globals = undefined;

            if (angular.isUndefined($localStorage.globals)) {
                console.log('DENY');
                $rootScope.globals = {
                    isLogin: 0
                };

                if ($location.path() == '/forget-password') {
                    $location.path('/forget-password');
                } else if ($location.path() == '/register') {
                    $location.path('/register');
                } else {
                    $location.path('/login');
                }

            } else {
                $http.defaults.headers.common['acess_token'] = $localStorage.globals.user_info.acess_token;
               
                $rootScope.globals = $localStorage.globals;
                if ($location.path() == '/login') {
                    $location.path('/home');
                }
            }

        });


        // Call Event on Page Change
        $rootScope.$on('$routeChangeStart', function(event, current, previous) {
            /* ACL corresponding to Pages Navigation and SubSection*/
            $rootScope.acl = {};
            $rootScope.acl.search_employee = aclService.checkACL('Employee','Search Employee',[1,2,3,4]);
            $rootScope.acl.add_employee = aclService.checkACL('Employee','Employee',[2,3]);
            $rootScope.acl.edit_employee_basic = aclService.checkACL('Employee','Basic Detail',[2,4]);
            $rootScope.acl.edit_employee_posting = aclService.checkACL('Employee','Posting Detail',[2,4]);
            $rootScope.acl.edit_employee_exp_and_qual = aclService.checkACL('Employee','Experience & Qualification',[2,4]);
            $rootScope.acl.edit_employee_account = aclService.checkACL('Employee','Account Detail',[2,4]);
            $rootScope.acl.edit_employee_leave = aclService.checkACL('Employee','Leave Detail',[2,4]);
            $rootScope.acl.edit_employee_ctc = aclService.checkACL('Employee','CTC',[2,4]);
            $rootScope.acl.edit_employee_document = aclService.checkACL('Employee','Documents',[2,4]);
            $rootScope.acl.edit_employee_ctc = aclService.checkACL('Employee', 'CTC', [2, 4]);

            /* ACL for Control Pages */
            $rootScope.acl.user_access = aclService.checkACL('Control and Settings','User Access Control',[2,3]);
            $rootScope.acl.recomended_approval_control = aclService.checkACL('Control and Settings','Recommended and Approval Control',[2,3]);
            $rootScope.acl.cpl_ot_eligibility = aclService.checkACL('Control and Settings','CPL/OT Eligibility Control',[2,3]);

            //leave section
            $rootScope.acl.apply_leave = aclService.checkACL('Leave', 'Leave', [2]);
            $rootScope.acl.control_leave_Recomended = aclService.checkACL('Leave', 'Leave Recommend', [2]);
            $rootScope.acl.control_leave_Approval = aclService.checkACL('Leave', 'Leave Approval', [2]);
            if ($rootScope.acl.control_leave_Approval == true && $rootScope.acl.control_leave_Recomended == true) {
                $rootScope.acl.control_leave = false;
                $rootScope.acl.control_leave_search = true;
            } else if ($rootScope.acl.control_leave_Approval == true || $rootScope.acl.control_leave_Recomended == true) {
                $rootScope.acl.control_leave = true;
                $rootScope.acl.control_leave_search = false;
            }
           


           
            //leave section
        });
        // Call Event on Page Change
        $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
            // Load masters data
            userService.Settings().then(function(response) {
                $rootScope.masters = response;

                console.log("-- Globals Data Start --");
                console.log($rootScope.globals);
                console.log("-- Globals Data End --");

                console.log("-- Masters Data Start --");
                console.log($rootScope.masters);
                console.log("-- Masters Data End --");
            });

            

            /* Set and Change Page Title */
            if (current.hasOwnProperty('$$route')) {

                $rootScope.title = current.$$route.title;
                $rootScope.pageType = current.$$route.pageType;


            }
        });

        // If the route change failed due to our "Unauthorized" error, redirect them
        $rootScope.$on('$routeChangeError', function(event, current, previous, rejection){

            console.log(rejection);
            if(rejection === 'Unauthorized'){
              $location.path('/');
            }
        });

        /* Check ACL in View Files */
        $rootScope.checkACL = function(Module,SubModule,Access){
           return aclService.checkACL(Module,SubModule,Access);
        };
    

    });
    