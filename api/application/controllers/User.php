<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
class User extends REST_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('user_model');
    }

	public function login_post(){
		
			$username = $this->post('username');
	        $password = $this->post('password');
			$user = $this->user_model->login($username,$password);
			if(!empty($user)){
				$user[0]->access_token = 123;

				$this->response([
					'data' => [
						'user_info'=>$user[0]
					],
	            	'message_type' => 'success',
	            	'message' => 'Successfully login'
	        	], REST_Controller::HTTP_OK);
			} else {
				$this->response([
	            	'message_type' => 'error',
	            	'message' => 'Username or Passsword Wrong'
	        	], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
			}
		
	
	}

	public function logout_post(){
		
			

				$this->response([
					
	            	'message_type' => 'success',
	            	'message' => 'Successfully login'
	        	], REST_Controller::HTTP_OK);
			
		
	
	}

	public function masters_post(){
		
			$username = $this->post('username');
	        $password = $this->post('password');
			$user = $this->user_model->login($username,$password);
			if(!empty($user)){
				$user[0]->access_token = 123;

				$this->response([
					'data' => [
						'user_info'=>$user[0]
					],
	            	'message_type' => 'success',
	            	'message' => 'Successfully login'
	        	], REST_Controller::HTTP_OK);
			} else {
				$this->response([
	            	'message_type' => 'error',
	            	'message' => 'Username or Passsword Wrong'
	        	], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
			}
	}

	public function countries_post(){
		$countries = $this->user_model->get_countries();
		if(!empty($countries)){
			$this->response([
				'data' => [
					'countries'=>$countries
				],
            	'message_type' => 'success',
            	'message' => 'Success'
        	], REST_Controller::HTTP_OK);
		} else {
			$this->response([
            	'message_type' => 'error',
            	'message' => 'error'
        	], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
		}
	}

	public function states_post(){

		$country_id = $this->post('country_id');
		$states = $this->user_model->get_states($country_id);
		if(!empty($states)){
			$this->response([
				'data' => [
					'states'=>$states
				],
            	'message_type' => 'success',
            	'message' => 'Success'
        	], REST_Controller::HTTP_OK);
		} else {
			$this->response([
            	'message_type' => 'error',
            	'message' => 'error'
        	], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
		}
	}

	public function cities_post(){
		$state_id = $this->post('state_id');
		$cities = $this->user_model->get_cities($state_id);
		if(!empty($cities)){
			$this->response([
				'data' => [
					'cities'=>$cities
				],
            	'message_type' => 'success',
            	'message' => 'Success'
        	], REST_Controller::HTTP_OK);
		} else {
			$this->response([
            	'message_type' => 'error',
            	'message' => 'error'
        	], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
		}
	}
}
