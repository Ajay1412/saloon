<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
class Saloon extends REST_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('saloon_model');
    }

	public function insert_post(){
		$data = $this->post();
		$saloon = $this->saloon_model->insert($data);

		if(!empty($saloon)){
			$this->response([
				'message_type' => 'success',
            	'message' => 'Successfully added'
        	], REST_Controller::HTTP_OK);
		} else {
			$this->response([
            	'message_type' => 'error',
            	'message' => 'Error'
        	], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
		}
	}

	public function list_post(){
		$filters = array();

		if(!empty($this->post('saloon_name'))){
			$filters['saloon_name'] = $this->post('saloon_name');
		}
		

		$page = $this->post('page');
		$limit = $this->post('limit'); 
		$order_by = $this->post('order_by'); 
		$order = $this->post('order'); 
		
		$saloons = $this->saloon_model->list($filters,$page,$limit,$order_by,$order);
		if(!empty($saloons)){
			$this->response([
				'data' => [
					'saloons'=>$saloons['result'],
					'total_rows'=>$saloons['total_rows'],
				],
				'message_type' => 'success',
            	'message' => 'Successfully'
        	], REST_Controller::HTTP_OK);
		} else {
			$this->response([
            	'message_type' => 'error',
            	'message' => 'Error'
        	], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
		}
	}

}
