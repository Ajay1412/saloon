<?php 
class Saloon_model extends CI_Model {

        public function __construct(){
	       // parent::__construct();
	        
	    }

        public function insert($data){
            $this->db->set($data);
            $result = $this->db->insert('saloons');
           // echo $this->db->last_query(); exit;
            return $result;
        }

        public function list($filters = array(),$page,$limit,$order_by,$order){
            $this->db->start_cache();
            $this->db->select('s.saloon_id,s.saloon_name,s.status,c.country_id,c.title as country,st.name as state');
            $this->db->from('saloons as s');
            $this->db->join('countries as c', 's.country_id = c.country_id', 'inner');
            $this->db->join('states as st', 's.state_id = st.state_id', 'inner');
            $this->db->where('s.status',1);
            $this->db->where($filters);
            $this->db->order_by($order_by,$order);
          
            $total_rows =  $this->db->count_all_results();
            
            $result =  $this->db->limit($limit,($page-1)*$limit)->get()->result_object();
            return array(
                'total_rows' => $total_rows,
                'result'     => $result,
            );


        }

        
} ?>