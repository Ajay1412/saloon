<?php 
class User_model extends CI_Model {

        public function __construct(){
	       // parent::__construct();
	        
	    }

        public function login($username,$password){
        		$this->db->select('*');
				$this->db->from('users');
				$this->db->where('username', $username);
				$this->db->where('password', $password);
				
				$query = $this->db->get();
        		$result = $query->result_object();
        		//echo $this->db->last_query();
        		return $result;
        }

        public function get_countries(){
    		$this->db->select('country_id,title,status');
			$this->db->from('countries');
			$this->db->where('status',1);

			$query = $this->db->get();
    		$result = $query->result_object();
    		return $result;
        }

        public function get_states($country_id){
    		$this->db->select('state_id,name,status');
			$this->db->from('states');
			$this->db->where('country_id',$country_id);
			$this->db->where('status',1);

			$query = $this->db->get();
    		$result = $query->result_object();
    		return $result;
        }

        public function get_cities($state_id){
    		$this->db->select('city_id,name,status');
			$this->db->from('cities');
			$this->db->where('state_id',$state_id);
			$this->db->where('status',1);

			$query = $this->db->get();
    		$result = $query->result_object();
    		//echo $this->db->last_query();
    		return $result;
        }

       

}
?>